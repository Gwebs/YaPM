"""
YaPM

Yet Another Package Manager,
We're gonna install windows software !

Author: Gabriel Jean
Website: https://gwebs.ca
Last edited: 2018-05-07
GitLab Integration test
"""

import sys, os, urllib.request, requests, subprocess, ctypes
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QCheckBox, QMessageBox, QAction, QLabel
from PyQt5.QtGui import QIcon

listcb = []
listlabel = []
listinstall = []

# Look online to look at what can be install
def initme():
    data = requests.get("https://animesenpai.ca/app.json")
    data = data.json()
    return data

try:
    tmp = os.getenv("TEMP")
except:
    print("Can't found the path to TMP")
    sys.exit()

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

#A bit of cleaning and show the results
def afterinstall():
    soft = ""
    count = len(listinstall)
    if count == 1:
        soft += listinstall[0]
        listlabel[0].setText(soft + " Is now Installed")
    else:
        for i in range(count):
            soft += listinstall[i] + " & "
        soft = soft[:-3]
        listlabel[0].setText(soft + " Are now Installed")
    listinstall.clear()

    for cb in listcb:
        if cb.isChecked():
            cb.setChecked(False)
            cb.hide()
#Install the software
def install(soft):
    path = tmp + "\\" + soft.text() + ".exe"
    if is_admin():
        print("Installation of : " + path)
        args = data[soft.text()]['args']
        subprocess.run(args=[path, args])
        print("Installation of : " + path + " Is now completed")
        listinstall.append(soft.text())
    else:
        mb = QMessageBox()
        mb.setText("Please run it as an administrator\n"
                   "YaPM is now closing !")
        mb.setWindowTitle("Error !")
        mb.setWindowIcon(QIcon("ico.ico"))
        mb.exec()
        sys.exit()

#Download the software
def Downloads():
    for soft in listcb:
        if soft.isChecked():
            print("Starting Download")
            urllib.request.urlretrieve(data[soft.text()]['url'], tmp + "\\" + soft.text() + ".exe")
            print("Download Complete")
            print(tmp + "\\" + soft.text() + ".exe")
            print("Starting Installation")
            install(soft)
    afterinstall()

#Creating the message box for the about me page
def about():
    mb = QMessageBox()
    mb.setTextFormat(1)
    mb.setText("YaPM <br> <br>Yet Another Package Manager <br> Author: Gabriel Jean"
               "<br> My Website : <a href='https://gwebs.ca'>https://gwebs.ca</a> <br> Last edited: 2018-05-07")
    mb.exec()

class Ui_MainWindow(object):

    #UI constructor
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(280, 363)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btn_install = QtWidgets.QPushButton(self.centralwidget)
        self.btn_install.setGeometry(QtCore.QRect(110, 10, 61, 21))
        self.btn_install.setObjectName("btn_install")
        self.btn_install.clicked.connect(Downloads)
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 60, 241, 251))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.cb_layout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.cb_layout.setContentsMargins(0, 0, 0, 0)
        self.cb_layout.setObjectName("cb_layout")
        self.result = QLabel()
        self.result.setText("Please Select your programs")
        listlabel.append(self.result)
        self.cb_layout.addWidget(self.result)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 280, 18))
        self.menubar.setObjectName("menubar")
        self.menuOptions = QtWidgets.QMenu(self.menubar)
        self.menuOptions.setObjectName("menuOptions")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.aboutaction = QAction("About YaPM")
        self.aboutaction.triggered.connect(about)
        self.menuHelp.addAction(self.aboutaction)
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuOptions.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.retranslateUi(MainWindow)
        MainWindow.setWindowIcon(QIcon("ico.ico"))
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # CheckBoxe Generator
        listsoft = data
        for soft in listsoft:
            cb = QCheckBox(soft)
            cb.setObjectName("salut")
            self.cb_layout.addWidget(cb)
            listcb.append(cb)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "YaPM"))
        self.btn_install.setText(_translate("MainWindow", "Install"))
        self.menuOptions.setTitle(_translate("MainWindow", "Options"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))

if __name__ == "__main__":
    import sys
    data = initme()
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

